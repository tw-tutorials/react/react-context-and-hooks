export interface Theme {
  syntax: string;
  ui: string;
  bg: string;
}