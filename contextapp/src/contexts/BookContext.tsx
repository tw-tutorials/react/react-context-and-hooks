import React, { createContext, useState } from 'react';

interface Book {
	id: number;
	title: string;
}

interface BookState {
	books: Book[];
}
const initState: BookState = {
	books: [],
};

interface ContextType extends BookState {}
const defaultValue: ContextType = {
	...initState,
};

export const BookContext = createContext<ContextType>(defaultValue);

const BookContextProvider: React.FC = (props) => {
	const [books, setBooks] = useState<Book[]>([
		{ id: 1, title: 'name of the wind' },
		{ id: 2, title: 'the way of kings' },
		{ id: 3, title: 'the final empire' },
		{ id: 4, title: 'the hero of ages' },
	]);

	return <BookContext.Provider value={{ books }}>{props.children}</BookContext.Provider>;
};

export default BookContextProvider;
