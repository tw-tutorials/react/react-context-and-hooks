import React, { createContext, Component } from 'react';
import { Theme } from '../interfaces/Theme';

interface ThemeState {
	isLightTheme: boolean;
	light: Theme;
	dark: Theme;
}
const initState: ThemeState = {
	isLightTheme: true,
	light: { syntax: '#555', ui: '#ddd', bg: '#eee' },
	dark: { syntax: '#ddd', ui: '#333', bg: '#555' },
};

interface ContextType extends ThemeState {
	toggleTheme: () => void;
}
const initValue: ContextType = {
	...initState,
	toggleTheme: () => {},
};

export const ThemeContext = createContext<ContextType>(initValue);

class ThemeContextProvider extends Component<{}, ThemeState> {
	state: ThemeState = initState;

	toggleTheme = () => {
		this.setState({ isLightTheme: !this.state.isLightTheme });
	};

	render() {
		return (
			<ThemeContext.Provider value={{ ...this.state, toggleTheme: this.toggleTheme }}>
				{/* Children */}
				{this.props.children}
			</ThemeContext.Provider>
		);
	}
}

export default ThemeContextProvider;
