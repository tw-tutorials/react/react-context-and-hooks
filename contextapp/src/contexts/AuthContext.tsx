import React, { createContext, Component } from 'react';

interface AuthState {
	isAuthenticated: boolean;
}
const initState: AuthState = {
	isAuthenticated: false,
};

interface ContextType extends AuthState {
	toggleAuth: () => void;
}
const defaultValue: ContextType = {
	...initState,
	toggleAuth: () => {},
};

export const AuthContext = createContext<ContextType>(defaultValue);

class AuthContextProvider extends Component<{}, AuthState> {
	state: AuthState = initState;

	toggleAuth = () => {
		this.setState({ isAuthenticated: !this.state.isAuthenticated });
	};

	render() {
		return (
			<AuthContext.Provider value={{ ...this.state, toggleAuth: this.toggleAuth }}>
				{/* Children */}
				{this.props.children}
			</AuthContext.Provider>
		);
	}
}

export default AuthContextProvider;
