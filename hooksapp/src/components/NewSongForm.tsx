import React, { useState } from 'react';

const NewSongForm: React.FC<{ addSong: (title: string) => void }> = ({ addSong }) => {
	const [title, setTitle] = useState<string>('');

	const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		addSong(title);
		setTitle('');
	};

	return (
		<form onSubmit={handleSubmit}>
			<label htmlFor="name">Song name: </label>
			<input
				type="text"
				id="name"
				required
				value={title}
				onChange={(e) => setTitle(e.target.value)}
			/>

			<input type="submit" value="add song" />
		</form>
	);
};

export default NewSongForm;
