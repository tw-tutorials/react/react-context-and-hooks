import React, { useState, useEffect } from 'react';
import { v4 as uuid } from 'uuid';
import { Song } from '../interfaces/Song';
import NewSongForm from './NewSongForm';

const SongList: React.FC = () => {
	const [songs, setSongs] = useState<Song[]>([
		{ id: 1, title: 'almost home' },
		{ id: 2, title: 'memory gospel' },
		{ id: 3, title: 'this wild darkness' },
	]);
	const [age, setAge] = useState<number>(20);

	useEffect(() => {
		console.log('useEffect hook ran', songs);
	}, [songs]);
	useEffect(() => {
		console.log('useEffect hook ran', age);
	}, [age]);

	const addSong = (title: string) => {
		setSongs([...songs, { id: uuid(), title }]);
	};

	return (
		<div className="song-list">
			<ul>
				{songs.map((song) => (
					<li key={song.id}>{song.title}</li>
				))}
			</ul>

			<NewSongForm addSong={addSong} />

			<button onClick={() => setAge(age + 1)}>Add 1 to age: {age}</button>
		</div>
	);
};

export default SongList;
