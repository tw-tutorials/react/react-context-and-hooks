export interface Song {
  id: number | string;
  title: string;
}