import React, { useContext, useState } from 'react';
import { BookContext } from '../contexts/BookContext';

const NewBookForm: React.FC = () => {
	const { dispatch } = useContext(BookContext);

	const [title, setTitle] = useState('');
	const [author, setAuthor] = useState('');

	const handleSubmit = (e: React.FormEvent) => {
		e.preventDefault();
		dispatch({ type: 'ADD_BOOK', book: { title, author } });

		setTitle('');
		setAuthor('');
	};

	return (
		<form onSubmit={handleSubmit}>
			<input
				type="text"
				placeholder="Book title"
				value={title}
				onChange={(e) => setTitle(e.target.value)}
				required
			/>
			<input
				type="text"
				placeholder="Author"
				value={author}
				onChange={(e) => setAuthor(e.target.value)}
				required
			/>

			<button type="submit">Add book</button>
		</form>
	);
};

export default NewBookForm;
