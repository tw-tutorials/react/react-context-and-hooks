import { Book } from '../contexts/BookContext';
import { v4 as uuid } from 'uuid';

export const bookReducer = (state: Book[], action: any) => {
	switch (action.type) {
		case 'ADD_BOOK':
			return [
				...state,
				{
					id: uuid(),
					title: action.book.title,
					author: action.book.author,
				},
			];
		case 'REMOVE_BOOK':
			return state.filter((b) => b.id !== action.id);

		default:
			return state;
	}
};
