import React, { createContext, useReducer, useEffect } from 'react';
import { bookReducer } from '../reducers/bookReducer';

export interface Book {
	id: string | number;
	title: string;
	author: string;
}

export interface BookState {
	books: Book[];
}
const initState: BookState = {
	books: [],
};

interface ContextType extends BookState {
	dispatch: any;
}
const defaultValue: ContextType = {
	...initState,
	dispatch: null,
};

export const BookContext = createContext<ContextType>(defaultValue);

const BookContextProvider: React.FC = (props) => {
	const [books, dispatch] = useReducer(bookReducer, [], () => {
		const localData = localStorage.getItem('books');
		return localData ? JSON.parse(localData) : [];
	});

	useEffect(() => {
		localStorage.setItem('books', JSON.stringify(books));
	}, [books]);

	return (
		<BookContext.Provider value={{ books, dispatch }}>{props.children}</BookContext.Provider>
	);
};

export default BookContextProvider;
